# kubectl-mtail

This repository contains a `kubectl plugin` called `mtail`, which tail logs from multiple pods matching label selector.


You can install this plugin on your machine with `krew` plugin manager:
https://github.com/GoogleContainerTools/krew
```
kubectl krew install mtail
```
